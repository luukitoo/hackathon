package com.example.healthtech

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.cardview.widget.CardView

class MainActivity : AppCompatActivity() {

    private lateinit var breakfastButton: CardView
    private lateinit var lunchButton: CardView
    private lateinit var dinnerButton: CardView
    private lateinit var snackButton: CardView
    private lateinit var foodIntent: Intent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()

        onClickListeners()

    }

    private fun onClickListeners() {
        breakfastButton.setOnClickListener {
            foodIntent.putExtra("name", "საუზმე")
            startActivity(foodIntent)
        }
        lunchButton.setOnClickListener {
            foodIntent.putExtra("name", "სადილი")
            startActivity(foodIntent)
        }
        dinnerButton.setOnClickListener {
            foodIntent.putExtra("name", "ვახშამი")
            startActivity(foodIntent)
        }
        snackButton.setOnClickListener {
            foodIntent.putExtra("name", "სნეკი")
            startActivity(foodIntent)
        }
    }

    private fun init() {
        breakfastButton = findViewById(R.id.breakfastButton)
        lunchButton = findViewById(R.id.lunchButton)
        dinnerButton = findViewById(R.id.dinnerButton)
        snackButton = findViewById(R.id.snackButton)
        foodIntent = Intent(this, FoodActivity::class.java)
    }

}