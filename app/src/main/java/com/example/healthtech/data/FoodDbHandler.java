package com.example.healthtech.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.healthtech.models.Food;
import com.example.healthtech.utils.FoodUtils;

import java.util.ArrayList;
import java.util.List;

public class FoodDbHandler extends SQLiteOpenHelper {
    public FoodDbHandler(Context context) {
        super(context, FoodUtils.DATABASE_NAME, null, FoodUtils.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE " + FoodUtils.TABLE_NAME + "("
                        + FoodUtils.KEY_ID + "INTEGER PRIMARY KEY, "
                        + FoodUtils.KEY_IMAGE + "INTEGER, "
                        + FoodUtils.KEY_NAME + "TEXT, "
                        + FoodUtils.KEY_CALORIES + "TEXT, "
                        + FoodUtils.KEY_FAT + "TEXT, "
                        + FoodUtils.KEY_PROTEIN + "TEXT, "
                        + FoodUtils.KEY_CARBOHYDRATES + "TEXT)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + FoodUtils.TABLE_NAME);
        onCreate(db);
    }

    public void addFood(Food food) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(FoodUtils.KEY_IMAGE, food.getFoodImage());
        values.put(FoodUtils.KEY_NAME, food.getName());
        values.put(FoodUtils.KEY_CALORIES, food.getCalories());
        values.put(FoodUtils.KEY_FAT, food.getFat());
        values.put(FoodUtils.KEY_PROTEIN, food.getProtein());
        values.put(FoodUtils.KEY_CARBOHYDRATES, food.getCarbohydrates());

        db.insert(FoodUtils.TABLE_NAME, null, values);
        db.close();

    }

    public List<Food> getAllFood() {
        SQLiteDatabase db = this.getReadableDatabase();

        List<Food> foodList = new ArrayList<>();

        String command = "SELECT * FROM " + FoodUtils.TABLE_NAME;
        Cursor cursor = db.rawQuery(command, null);

        if (cursor.moveToFirst()) {
            do {
                Food food = new Food();
                food.setId(cursor.getInt(0));
                food.setFoodImage(cursor.getInt(1));
                food.setName(cursor.getString(2));
                food.setCalories(cursor.getString(3));
                food.setFat(cursor.getString(4));
                food.setProtein(cursor.getString(5));
                food.setCarbohydrates(cursor.getString(6));
                foodList.add(food);
            } while (cursor.moveToNext());
        }
        return foodList;
    }

    public void deleteFood(String title) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(FoodUtils.TABLE_NAME, FoodUtils.KEY_NAME + "=?", new String[] {title});
        db.close();
    }

}
