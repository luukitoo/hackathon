package com.example.healthtech

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.healthtech.adapters.RecyclerViewFoodAdapter
import com.example.healthtech.models.Food
import java.util.ArrayList

class AddFoodActivity : AppCompatActivity() {

    private lateinit var backButton: ImageView
    private lateinit var rcView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_food)

        init()

        onClickListeners()

        val foodList = ArrayList<Food>()

        foodList.add(
            Food(
                R.drawable.egg, "Egg", "15 kcal", "14 g", "11 g", "0.8 g"
            )
        )

        rcView.adapter = RecyclerViewFoodAdapter(foodList)
        rcView.layoutManager = LinearLayoutManager(this)


    }

    private fun init() {
        backButton = findViewById(R.id.backButton)
        rcView = findViewById(R.id.rcView)
    }

    private fun onClickListeners() {
        backButton.setOnClickListener {
            finish()
        }
    }

}