package com.example.healthtech.utils;

public class FoodUtils {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "HealthTechDb";
    public static final String TABLE_NAME = "savedFood";

    public static final String KEY_ID = "id";
    public static final String KEY_IMAGE = "image";
    public static final String KEY_NAME = "name";
    public static final String KEY_CALORIES = "calories";
    public static final String KEY_FAT = "fat";
    public static final String KEY_PROTEIN = "protein";
    public static final String KEY_CARBOHYDRATES = "carbohydrates";

}
