package com.example.healthtech

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.healthtech.adapters.RecyclerViewFoodAdapter
import com.example.healthtech.data.FoodDbHandler
import com.example.healthtech.models.Food

class FoodActivity2 : AppCompatActivity() {

    private lateinit var rcView: RecyclerView
    private lateinit var backButton: ImageView
    private lateinit var addButton: ImageView
    private lateinit var activityName: TextView
    private lateinit var startTextView: TextView
    private lateinit var dbHandler: FoodDbHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_food2)

        init()

        onClickListeners()

        val foodList: List<Food> = dbHandler.allFood

        if (foodList.isNotEmpty()) {
            startTextView.visibility = View.GONE
        }

        Log.d("list", "Size is: ${foodList.size}")

        rcView.adapter = RecyclerViewFoodAdapter(foodList)
        rcView.layoutManager = LinearLayoutManager(this)
    }

    private fun init() {
        rcView = findViewById(R.id.RcView)
        addButton = findViewById(R.id.addButton)
        backButton = findViewById(R.id.backButton)
        activityName = findViewById(R.id.textView2)
        startTextView = findViewById(R.id.startTextView)
        dbHandler = FoodDbHandler(this)
        activityName.text = intent.getStringExtra("name")
    }

    private fun onClickListeners() {
        addButton.setOnClickListener {
            startActivity(Intent(this, AddFoodActivity::class.java))
        }

        backButton.setOnClickListener {
            finish()
        }
    }
}