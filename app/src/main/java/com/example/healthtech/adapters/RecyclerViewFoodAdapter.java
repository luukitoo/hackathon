package com.example.healthtech.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.healthtech.R;
import com.example.healthtech.data.FoodDbHandler;
import com.example.healthtech.models.Food;

import org.w3c.dom.Text;

import java.util.List;

public class RecyclerViewFoodAdapter extends RecyclerView.Adapter<RecyclerViewFoodAdapter.FoodViewHolder> {
    private List<Food> foodList;
    private Context context;

    public RecyclerViewFoodAdapter(List<Food> foodList) {
        this.foodList = foodList;
    }

    @NonNull
    @Override
    public FoodViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.food_item, parent, false);
        context = parent.getContext();
        return new FoodViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FoodViewHolder holder, int position) {
        setData(holder, foodList.get(position));
    }

    @Override
    public int getItemCount() {
        return foodList.size();
    }

    public static class FoodViewHolder extends RecyclerView.ViewHolder {
        ImageView foodImage, saveButton;
        TextView foodName, calories, fat, protein, carbohydrates;
        ConstraintLayout appendButton;
        public FoodViewHolder(@NonNull View itemView) {
            super(itemView);
            foodImage = itemView.findViewById(R.id.foodImage);
            foodName = itemView.findViewById(R.id.textView);
            calories = itemView.findViewById(R.id.calories);
            fat = itemView.findViewById(R.id.fat);
            protein = itemView.findViewById(R.id.protein);
            carbohydrates = itemView.findViewById(R.id.carbohydrates);
            appendButton = itemView.findViewById(R.id.appendButton);
            saveButton = itemView.findViewById(R.id.saveButton);
        }
    }

    private void setData(FoodViewHolder holder, Food item) {
        holder.foodImage.setImageResource(item.getFoodImage());
        holder.foodName.setText(item.getName());
        holder.calories.setText(item.getCalories());
        holder.fat.setText(item.getFat());
        holder.protein.setText(item.getProtein());
        holder.carbohydrates.setText(item.getCarbohydrates());
        SharedPreferences sharedPreferences =
                context.getSharedPreferences("saved", Context.MODE_PRIVATE);
        if (sharedPreferences.getBoolean(holder.foodName.getText().toString(), false)) {
            holder.saveButton.setImageResource(R.drawable.ic_baseline_cross_24);
        }

        holder.appendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FoodDbHandler databaseHandler = new FoodDbHandler(v.getContext());
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean(item.getName(), false);
                editor.apply();
                if (!(sharedPreferences.getBoolean(item.getName(), false))) {
                    editor = sharedPreferences.edit();
                    editor.putBoolean(item.getName(), true);
                    editor.apply();
                    databaseHandler.addFood(new Food(item.getId(), item.getFoodImage(), item.getName(),
                            item.getCalories(), item.getFat(), item.getProtein(), item.getCarbohydrates()));
                    holder.saveButton.setImageResource(R.drawable.ic_baseline_cross_24);
                    Toast.makeText(context, "Food Saved", Toast.LENGTH_SHORT).show();
                } else {
                    editor = sharedPreferences.edit();
                    editor.putBoolean(item.getName(), false);
                    editor.apply();
                    databaseHandler.deleteFood(item.getName());
                    holder.saveButton.setImageResource(R.drawable.ic_baseline_add_24);
                    Toast.makeText(context, "Food Removed", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
